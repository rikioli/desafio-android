package android.com.br.desafioandroid.shotList;

import android.com.br.desafioandroid.data.model.ShotModel;

import java.util.List;

/**
 * Created by henriquecoliveira on 12/04/17.
 */

public interface ShotListContract {

    interface View{

        void getListSuccess(List<ShotModel> list);

        void getNextPageSuccess(ShotModel item);

        void showWait();

        void removeWait();

    }

    interface Presenter{
        void getShots(String url);
    }

    interface onScrollMore{
        void onLoadMorePages();
    }

}
