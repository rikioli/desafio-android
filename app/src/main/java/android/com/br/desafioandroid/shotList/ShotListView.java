package android.com.br.desafioandroid.shotList;

import android.com.br.desafioandroid.R;
import android.com.br.desafioandroid.data.model.ShotModel;
import android.com.br.desafioandroid.shotDetail.ShotDetailView;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

import java.util.List;

import io.fabric.sdk.android.Fabric;


public class ShotListView extends AppCompatActivity implements ShotListContract.View, ShotListContract.onScrollMore{


    private static final String TWITTER_KEY = "u3dBgAmDNz25nmJ1z4hQ6H25f";
    private static final String TWITTER_SECRET = "5XuQjLtWC8AondWTSWQ6x9wYqoGvYNZngWSivrc6qEZ3nwnD1Y";

    private static final String BUNDLE_RECYCLER_LAYOUT = "recycler.bundle";
    private ShotListPresenter mPresenter;
    SwipeRefreshLayout swipeRefreshLayout;
    RecyclerView recyclerView;
    ProgressBar progressBar;
    ShotAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_shot_list);

        initViews(savedInstanceState);
        mPresenter = new ShotListPresenter(this, this);
        mPresenter.getShots(null);
    }

    private void initViews(Bundle savedInstanceState) {



        progressBar = (ProgressBar) findViewById(R.id.progress);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh_layout);

        GridLayoutManager mLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
        OnScrollMoreRecyclerview endlessScroll = new OnScrollMoreRecyclerview(mLayoutManager);
        endlessScroll.setListener(this);
        recyclerView.addOnScrollListener(endlessScroll);


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getShots(null);
            }
        });

        if(savedInstanceState != null) {
            int savedRecyclerLayoutState = savedInstanceState.getInt(BUNDLE_RECYCLER_LAYOUT);
            (recyclerView.getLayoutManager()).scrollToPosition(savedRecyclerLayoutState);

        }

        PrimaryDrawerItem comments = new PrimaryDrawerItem().withIdentifier(1).withName("Order by comments");
        PrimaryDrawerItem recent = new PrimaryDrawerItem().withIdentifier(2).withName("Order by recent");
        PrimaryDrawerItem view = new PrimaryDrawerItem().withIdentifier(3).withName("Order by views");


        new DrawerBuilder()
                .withActivity(this)
                .withSavedInstance(savedInstanceState)
                .withDisplayBelowStatusBar(false)
                .withTranslucentStatusBar(false)
                .withDrawerLayout(R.layout.material_drawer_fits_not)
                .addDrawerItems(
                        view,
                        recent,
                        comments
                )
                .withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if(position == 0){
                            adapter.sortBy(new ShotModel.ComparatorByViews());
                            adapter.notifyDataSetChanged();
                        } else if(position == 1 ){
                            adapter.sortBy(new ShotModel.ComparatorByRecent());
                            adapter.notifyDataSetChanged();
                        } else if(position == 2){
                            adapter.sortBy(new ShotModel.ComparatorByComments());
                            adapter.notifyDataSetChanged();
                        }
                        return true;
                    }
                })
                .build();
    }

    @Override
    public void getListSuccess(List<ShotModel> list) {
         adapter = new ShotAdapter(getApplicationContext(), list,
                new ShotAdapter.OnItemClickListener() {
                    @Override
                    public void onClick(ShotModel shot) {

                        Intent intent = new Intent(ShotListView.this, ShotDetailView.class);
                        intent.putExtra(ShotDetailView.SHOT_ITEM_EXTRA, shot);
                        startActivity(intent);

                    }
                });

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void getNextPageSuccess(ShotModel item) {
        adapter.addShot(item);
        adapter.notifyItemInserted(adapter.getItemCount() - 1);
    }

    @Override
    public void showWait() {
        progressBar.setVisibility(View.VISIBLE);

    }

    @Override
    public void removeWait() {
        progressBar.setVisibility(View.INVISIBLE);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
        int lastFirstVisiblePosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstCompletelyVisibleItemPosition();
        outState.putInt(BUNDLE_RECYCLER_LAYOUT, lastFirstVisiblePosition);

    }

    @Override
    public void onLoadMorePages() {
        mPresenter.getShots("next");
    }
}


