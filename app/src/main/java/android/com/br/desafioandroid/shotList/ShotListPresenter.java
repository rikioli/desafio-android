package android.com.br.desafioandroid.shotList;

import android.com.br.desafioandroid.data.ApiController;
import android.com.br.desafioandroid.data.model.ShotModel;
import android.content.Context;
import android.util.Log;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by henriquecoliveira on 12/04/17.
 */

public class ShotListPresenter implements ShotListContract.Presenter {

    private final Context context;
    private final ShotListContract.View mView;
    private final ApiController mService;



    public ShotListPresenter(ShotListContract.View listener, Context context){
        this.mView = listener;
        this.context = context;
        this.mService = new ApiController();
    }

    @Override
    public void getShots(final String url){
        mView.showWait();

        Call<List<ShotModel>> call;

        if(url != null) {
            Log.d("Page", " " + mService.getNextPage());
            call = mService.service().getShots(mService.getNextPage());
        }
        else {

            call = mService.service().getShots();
        }

        call.enqueue(new Callback<List<ShotModel>>() {
                    @Override
                    public void onResponse(Call<List<ShotModel>> call, Response<List<ShotModel>> response) {
                        mView.removeWait();
                        if(response.isSuccessful()){

                            Headers responseHeaders = response.headers();
                            String links = responseHeaders.get("Link");

                            if(links != null) {
                                String nextPageLink;

                                Matcher m = Pattern.compile("\\<(.*?)\\>; rel=\"next\"").matcher(links);
                                while(m.find()) {
                                    nextPageLink = m.group(1);
                                    mService.setNextPage(nextPageLink);
                                    break;
                                }
                            }

                            if(url != null){
                                for (ShotModel shot : response.body()) {
                                    mView.getNextPageSuccess(shot);
                                }
                            } else {

                                mView.getListSuccess(response.body());
                            }
                        }



                    }

                    @Override
                    public void onFailure(Call<List<ShotModel>> call, Throwable t) {
                        try {
                            throw  new InterruptedException("Erro na comunicação com o servidor!");
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }
}
