package android.com.br.desafioandroid.shotList;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by henriquecoliveira on 12/04/17.
 */

public class OnScrollMoreRecyclerview extends RecyclerView.OnScrollListener {

    int firstVisibleItem, visibleItemCount, totalItemCount;
    private GridLayoutManager linearLayoutManager;
    private ShotListContract.onScrollMore listener;

    public OnScrollMoreRecyclerview(GridLayoutManager linearLayoutManager) {
        this.linearLayoutManager = linearLayoutManager;
    }

    public void setListener(ShotListContract.onScrollMore instance) {
        this.listener = instance;
    }


    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);

        visibleItemCount = recyclerView.getChildCount();
        totalItemCount = linearLayoutManager.getItemCount();
        firstVisibleItem = linearLayoutManager.findFirstVisibleItemPosition();

        if (firstVisibleItem + visibleItemCount >= totalItemCount) {
            listener.onLoadMorePages();
        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
    }
}
