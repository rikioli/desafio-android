package android.com.br.desafioandroid.shotDetail;

import android.com.br.desafioandroid.R;
import android.com.br.desafioandroid.data.model.ShotModel;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by henriquecoliveira on 13/04/17.
 */

public class ShotDetailView extends AppCompatActivity {
    public static final String SHOT_ITEM_EXTRA = "SHOT_ITEM_EXTRA";

    TextView author, description, title;
    ImageView image, avatar, facebook, twitter;
    ShotModel shotModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shot_detail);

        Intent intent = getIntent();
        shotModel = (ShotModel) intent.getSerializableExtra(SHOT_ITEM_EXTRA);

        initViews();



        if(shotModel != null) {
            setShot(shotModel);
        }
    }

    private void initViews() {
        author = (TextView) findViewById(R.id.author);
        description = (TextView) findViewById(R.id.description);
        title = (TextView) findViewById(R.id.title);
        image = (ImageView) findViewById(R.id.image);
        avatar = (ImageView) findViewById(R.id.avatar);
        facebook = (ImageView) findViewById(R.id.facebook_share);
        twitter = (ImageView) findViewById(R.id.twitter_share);

        final ShareDialog shareDialog = new ShareDialog(this);
        facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShareLinkContent.Builder builder = new ShareLinkContent.Builder();
                builder = builder.setContentUrl(Uri.parse(shotModel.html_url))
                        .setContentTitle(shotModel.title);
                shareDialog.show(builder.build());

            }
        });

        final TweetComposer.Builder builder = new TweetComposer.Builder(this);
        twitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                URL url = null;
                try {
                    url = new URL(shotModel.html_url);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
                builder.url(url)
                    .text(shotModel.title);
                builder.show();


            }
        });

    }

    void setShot (final ShotModel shot) {
        if(shot.description != null) {
            description.setText(Html.fromHtml(shot.description));
        }

        if(shot.user != null && shot.user.name != null) {
            author.setText(shot.user.name);
        }

        if(shot.title != null){
            title.setText(shot.title);
        }

        if(shot.user.avatar_url !=null){
            Glide.with(getApplicationContext())
                    .load(shot.user.avatar_url)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .skipMemoryCache(true)
                    .placeholder(R.drawable.progress_animation)
                    .crossFade()
                    .into(avatar);
        }

        if(shot.images != null && shot.images.teaser != null) {
            Glide.with(getApplicationContext())
                    .load(shot.images.hidpi)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .skipMemoryCache(true)
                    .placeholder(R.drawable.progress_animation)
                    .crossFade()
                    .into(image);
        }

}


}
