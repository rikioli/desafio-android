package android.com.br.desafioandroid.data;

import android.com.br.desafioandroid.util.Constants;
import android.com.br.desafioandroid.data.model.ShotModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Url;

/**
 * Created by henriquecoliveira on 12/04/17.
 */

public class ApiController {

        private static Retrofit retrofit;
        private String nextPage;

        public String getNextPage() {
            return nextPage;
        }

        public void setNextPage(String nextPage) {
        this.nextPage = nextPage;
    }

        public ApiController() {

            if (retrofit == null) {


                retrofit = new Retrofit.Builder()
                        .baseUrl(Constants.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }

        }

        public EndpointInterface service() {
            return retrofit.create(EndpointInterface.class);
        }

        public interface EndpointInterface{

            @Headers(Constants.API_OAUTH)
            @GET("/v1/shots")
            Call<List<ShotModel>> getShots();

            @Headers(Constants.API_OAUTH)
            @GET
            Call<List<ShotModel>> getShots(@Url String url);
        }
    }