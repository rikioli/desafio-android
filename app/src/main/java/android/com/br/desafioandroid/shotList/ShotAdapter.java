package android.com.br.desafioandroid.shotList;

import android.com.br.desafioandroid.R;
import android.com.br.desafioandroid.data.model.ShotModel;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by henriquecoliveira on 12/04/17.
 */

public class ShotAdapter extends RecyclerView.Adapter<ShotAdapter.ViewHolder> {
    private final OnItemClickListener listener;
    private List<ShotModel> list;
    private Context context;

    public ShotAdapter(Context context, List<ShotModel> list, OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public ShotAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_shot, null) ;
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ShotAdapter.ViewHolder holder, int position) {
        ShotModel shot = list.get(position);
        holder.click(shot, listener);

        holder.view.setText(Integer.toString(shot.views_count));
        holder.comment.setText(Integer.toString(shot.comments_count));
        holder.like.setText(Integer.toString(shot.likes_count));

        if(shot.images != null && shot.images.teaser != null) {

            Glide.with(context)
                    .load(shot.images.normal)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .skipMemoryCache(true)
                    .placeholder(R.drawable.progress_animation)
                     .crossFade()
                    .into(holder.image);

        }

    }

    public void addShot(ShotModel item) {
        list.add(item);
    }

    public void sortBy(Comparator<ShotModel> comparator) {
        Collections.sort(list, comparator);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public interface OnItemClickListener {
        void onClick(ShotModel Item);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView view,like,comment;

        public ViewHolder(View itemView) {
            super(itemView);

            image = (ImageView) itemView.findViewById(R.id.image);
            view = (TextView) itemView.findViewById(R.id.view_text);
            like = (TextView) itemView.findViewById(R.id.comment_text);
            comment = (TextView) itemView.findViewById(R.id.like_text);
        }


        public void click(final ShotModel item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(item);
                }
            });
        }
    }


}